export default interface coffeeMachine {
	// 제가 생각하는 커피머신에 꼭 필요한 구성입니다.
	name: string // 이름
	beanMount: number // 원두 양
	waterMount: number // 물의 양
	switchPower(): void // 전원 버튼
	drop(): void // 추출 버튼
}
