import scanf from 'scanf'
import coffeeLoader from './coffeeLoder'
import coffeeMachine from './coffeeMachine'

export default class CoffeMachineImpl implements coffeeMachine {
	name: string = 'Movv/ver.1.0'
	#waterLimit = 1000 //물 최대 용량
	#beanLimit = 1000 //원두 최대 용량
	beanMount = 0 //원두 양 초기 값으로 지정
	waterMount = 0 //물의 양 초기 값 지정
	menu: { [menuNum: string]: string } = {
		//메뉴 객체
		'1': '커피1잔',
		'2': '커피2잔',
		'3': '에스프레소1잔',
		'4': '에스프레소2잔',
	}
	private power = false
	private coffeeMaterial: Array<number> = [300, 100] // 커피 한잔에 필요한 물의양 200 원두양 100
	private essoprMaterial: Array<number> = [200, 50] //  에스프레소 한잔에 필요한  물의 양 원두양
	private oneShot = 50 // 샷 하나의 필요한 원두양
	private oneDensity = 50 // 농도 1단게 마다 줄어드는 물의 양
	private order: {
		// 총 주문들을 받아서 객체를  리턴
		shot: number
		density: number
		menuName: string
	} = { shot: 0, density: 0, menuName: '' }

	private reqCofWater = this.coffeeMaterial[0] - this.oneDensity * this.order.density // 커피를 한잔 만들 시 필요한 물의 양
	private reqEsoWater = this.essoprMaterial[0] - this.oneDensity * this.order.density // 에스프레소 한잔 만들시 필요한 물의 양
	private reqCofBean = this.coffeeMaterial[1] + this.oneShot * this.order.shot // 커피를 한잔 만들시 필요한 원두 양
	private reqEsoBean = this.essoprMaterial[1] + this.oneShot * this.order.shot // 에스프레소 한잔 만들 시 필요한 원두 양

	constructor(waterMount: number, beanMount: number) {
		// 커피 머신 생성
		this.waterMount = waterMount
		this.beanMount = beanMount
	}

	private log = (msg: string): void => {
		// 콘솔을 편리하기 사용하기 위한 함수
		console.log(msg)
	}

	private async delay(second: number, service: string): Promise<void> {
		// 서비스 별 로딩할 시간을 설정 합니다.
		this.log(service)
		return await new Promise<void>(res => {
			setTimeout(res, second)
		})
	}

	switchPower(): void {
		// 전원 버튼 켜기
		this.power = !this.power
	}

	private powerError(): void {
		// 전원을 안 켰을 시 에러
		throw new Error('POWER ERROR')
	}

	async powerCheck(): Promise<void> {
		// switchPower() 을 눌렀는지 체크를 하고 실행의 여부를 따진다.
		let powerOrderCheck: boolean = false
		await this.delay(4000, '====전원을 확인 중입니다====')
		powerOrderCheck = this.power
		if (powerOrderCheck) {
			this.log(`hello ${this.name} coffee machine~~ (* ㅇ *)`)
		} else {
			this.powerError()
		}
	}

	addShot(shot: number): boolean {
		// 커피의 샷을 추가 할 수 있습니다 default 는 0 입니다.
		let shotCheck = false // 샷 유효성 체크
		if (shot > 3 || shot < 0) {
			this.log('샷 추가는 총 0~3단계 까지 있습니다.')
		} else {
			this.order.shot = shot
			shotCheck = true
		}
		return shotCheck
	}

	selectDensity(density: number): boolean {
		// 커피의 농도를 선택할 수 있습니다. default는 1입니다.
		let densityCheck = false
		if (density > 3 || density < 0) {
			this.log('농도는 총 0~3단계 까지 있습니다.')
		} else {
			this.order.density = density
			densityCheck = true
		}
		return densityCheck
	}

	selectMenu(menuNum: string): boolean {
		// 메뉴를 선택할 수 있습니다 만약 메뉴에 없는 것들이 출력 된다면 log 실행 후 무한 에러로 보내버립니다.
		let menuCheck = false
		if (Object.keys(this.menu).includes(menuNum)) {
			this.order.menuName = this.menu[menuNum]
			menuCheck = true
		} else this.log('선택하신 주문은 메뉴에 없습니다.')
		return menuCheck
	}

	private waterCheck(menuName: string): boolean {
		// 커피와 에스프레소에 따라서 물의 양을 체크하는 함수
		let waterCheck = false //물의 양 상태

		if (menuName.includes('커피')) {
			if (this.waterMount < this.reqCofWater) {
				this.log(`물이 부족합니다. 현재 물의 양 : ${this.waterMount} 필요한 물의 양 : ${this.reqCofWater}`) //현재 물이 필요한 물 보다 적다면 콘솔로 표시
			} else waterCheck = true
		} else if (menuName.includes('에스프레소')) {
			if (this.waterMount < this.reqEsoWater) {
				this.log(`물이 부족합니다. 현재 물의 양 : ${this.waterMount} 필요한 물의 양 : ${this.reqEsoWater}`)
			} else waterCheck = true
		}
		return waterCheck
	}

	private chargeWater(): void {
		// 물 부족시 충전하는 함수
		while (true) {
			// 물 충전 시 최대 용량을 초과하면 다시 입력 받게 합니다.
			let water = scanf('%d')
			if (this.waterMount + water > this.#waterLimit) {
				this.log(`물의 최대 용량은 ${this.#waterLimit}ml 입니다`)
			} else {
				this.waterMount += water
				break
			}
		}
	}

	private consumeWater(menuName: string): number {
		// 커피와 에스프레소에 농도에 따른 필요한 물의 양을 계산하여 현재 물의 양을 반환한다.
		if (menuName.includes('커피')) this.waterMount -= this.reqCofWater
		else if (menuName.includes('에스프레소')) this.waterMount -= this.reqEsoWater

		return this.waterMount
	}

	private beanCheck(menuName: string): boolean {
		// 커피와 에스프레소에 따라 원두가 충분하지 파악하는 함수
		let beanCheck = false

		if (menuName.includes('커피')) {
			if (this.beanMount < this.reqCofBean) {
				this.log(`원두가 부족합니다. 현재 원두의 양 :${this.beanMount} 필요한 원두의 양 : ${this.reqCofBean} `) //현재 원두가 필요한 원두 양보다 적다면 콘솔로 표시
			} else beanCheck = true
		} else if (menuName.includes('에스프레소')) {
			if (this.beanMount < this.reqEsoBean) {
				this.log(`원두가 부족합니다. 현재 원두의 양 :${this.beanMount} 필요한 원두의 양 : ${this.reqEsoBean} `)
			} else beanCheck = true
		}
		return beanCheck
	}

	private chargeBean(): void {
		// 원두 부족시 충전하는 함수
		while (true) {
			// 원두 충족 시 최대 원두 양을 넘게 되면 다시 입력 받도록 합니다
			let bean = scanf('%d')
			if (this.beanMount + bean > this.#beanLimit) {
				this.log(`원두의 최대 용량은 ${this.#beanLimit}g 입니다.`)
			} else {
				this.beanMount += bean
				break
			}
		}
	}

	private consumeBean(menuName: string): number {
		// 커피와 에스프레소에 따라 필요한 원두를 사용하고 현재 원두를 반환한다.
		if (menuName.includes('커피')) this.beanMount -= this.reqCofBean
		else if (menuName.includes('에스프레소')) this.beanMount -= this.reqEsoBean

		return this.beanMount
	}

	private orderCheck(menuName: string): any {
		// 메뉴 이름에 1잔과 2잔에 따라 물 체크 와 원두 체크를 while문으로 돌린 후 체크가 완료되면 재료를 사용하는 함수 입니다.

		let orderCheck: boolean = false

		if (menuName.includes('1잔')) {
			// 들어온 메뉴의 (커피 혹은 에스프레소)가 1잔 일 때
			while (true) {
				// 물의 양을 계속 체크하며 필요한 물의 양보다 커지면 원두 체크로 넘어갑니다
				if (this.waterCheck(menuName)) break
				else {
					this.chargeWater()
				}
			}
			while (true) {
				// 원두의 양을 계속 체크하며 필요한 원두의 양보다 커지면 추출로 넘어갑니다.
				if (this.beanCheck(menuName)) break
				else {
					this.chargeBean()
				}
			}
			this.consumeWater(menuName)
			this.consumeBean(menuName)

			orderCheck = true // 이후 true 값을 반환하여 drop() 에서 추출 메시지를 띄워줍니다.
		} else if (menuName.includes('2잔')) {
			// 2잔일 때는 1잔과 같은 프로세스가 두번 반복합니다
			for (let cup = 0; cup < 2; cup++) {
				while (true) {
					if (this.waterCheck(menuName)) break
					else {
						this.chargeWater()
					}
				}
				while (true) {
					if (this.beanCheck(menuName)) break
					else {
						this.chargeBean()
					}
				}
				this.consumeWater(menuName)
				this.consumeBean(menuName)
			}
			orderCheck = true
		}
		return orderCheck
	}

	drop(): Promise<void> {
		// 주문을 받은 객체를 이용해서 전원의 여부를 확인하고 true 라면 orderCheck() 를 통해 추출의 가능성을 받는다
		return new Promise(res => {
			res(
				this.powerCheck().then(async () => {
					if (!this.power) return

					await this.delay(2000, '주문 확인 중')

					let orderCheck = this.orderCheck(this.order.menuName)
					if (orderCheck) {
						// 추출의 가능하면 완성된 커피를 출력한다.
						await this.delay(3000, '+++ Loding and Droping Coffee...+++')
						this.log(`농도 ${this.order.density} 단계, ${this.order.shot} 샷의 ${this.order.menuName}이 완성되었습니다 :)`)
						this.log(`=====현재 물 양: ${this.waterMount} 원두 양 : ${this.beanMount}===`)
					}
				})
			)
		})
	}
}

const movv = new CoffeMachineImpl(200, 200)
coffeeLoader(movv)
