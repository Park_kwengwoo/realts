import CoffeeMachineImpl from './cofMachinImpl' // 사용하고자 하는 커피머신 클래스를 가져왔습니다.
const scanf = require('scanf') // 입력을 받아 실행기를 동기적으로 작동하기 위해 설치한 라이브러리 (c언어 scanf 와 유사)

export default function coffeeLoader(coffeeMachine: CoffeeMachineImpl): void {
	// 커피머신이 입력한 대로 구동 되게 하기
	let waterInput = 0 // 물
	let beanInput = 0 // 원두
	let shotInput = 0 // 샷
	let densityInput = 0 // 농도
	let menuNumInput: string = '' // 메뉴 번호
	let next: string = '' // 실행 후 다음 순서로 넘어갈 수 있게 하기 위함

	const log = (msg: string): void => {
		console.log(msg)
	} // log 사용하기 편라히게 만든 함수
	const synch = (second: number, msg: void): Promise<void> => {
		// 실행기를 동기적으로 표현하기 위한 함수
		return new Promise(msg => {
			setTimeout(msg, second)
		})
	}
	async function retry(coffeeMachine: CoffeeMachineImpl): Promise<void> {
		// 재 주문을 받는 함수이며 load() 프로세스가 동일합니다.

		log('원하시는 메뉴의 번호를 입력해주세요') // 메뉴 선택
		for (let key in coffeeMachine.menu) {
			log(`주문 번호 ${key}: ${coffeeMachine.menu[key]}`)
		}
		while (true) {
			menuNumInput = scanf('%s')
			if (coffeeMachine.selectMenu(menuNumInput)) {
				log(coffeeMachine.menu[menuNumInput])
				break
			}
		}

		log('샷은 몇잔 추가 하시겠습니다?? 최대 3샷 까지 가능') // 샷 추가
		while (true) {
			shotInput = scanf('%d')
			if (coffeeMachine.addShot(shotInput)) break
		}

		log('농도를 몇단계 하시겠습니까? (총 1~3단계 까지 있음 기본 1단계)') // 농도 선택 후 추출
		while (true) {
			densityInput = scanf('%d')
			if (coffeeMachine.selectDensity(densityInput)) break
		}
		await coffeeMachine.drop() // 추출

		log('또 주문 하시겠습니까? (Y/N)') // 재시작
		next = scanf('%s')
		next === 'Y' ? retry(coffeeMachine) : log('====See you Later movv...')
	}

	async function load(): Promise<void> {
		// 커피머신을 실행하는 함수 입니다. (로딩 => 입력 => 객체 생성 => 메뉴 선택 => 추출 => 재시작)

		log('....애뮬레이터 실행') // 로딩
		await synch(1000, log('무브 직원이 들어와 커피 머신 앞에 있습니다.'))

		await synch(1000, log('먼저 물과 원두를 충전해주세요 (순서 물 , 원두)')) // 입력
		while (true) {
			// 양을 초과하게 된다면 다시 입력합니다.
			waterInput = scanf('%d')
			beanInput = scanf('%d')
			if (waterInput > 1000 || beanInput > 1000) {
				log('용량을 초과했습니다.')
				log('물 최대 1000ml 원두 최대 1000g 입니다.')
				log('다시 입력해주세요')
			} else break
		}

		log('...가동 중') // 커피머신 객체 생성
		await synch(2000, log('커피 머신이 작동합니다..'))
		coffeeMachine = new CoffeeMachineImpl(waterInput, beanInput)
		coffeeMachine.switchPower()

		log(`원하시는 메뉴의 번호를 입력해주세요`) //메뉴 선택
		for (let menuNum in coffeeMachine.menu) {
			log(`주문 번호 ${menuNum}: ${coffeeMachine.menu[menuNum]}`)
		}
		while (true) {
			// 메뉴를 잘못 선택할 시 재입력을 받습니다.
			menuNumInput = scanf('%s')
			if (coffeeMachine.selectMenu(menuNumInput)) {
				log(coffeeMachine.menu[menuNumInput])
				break
			}
		}

		log('샷은 몇잔 추가 하시겠습니다?? (총 0~3단계 까지)') //샷 추가
		while (true) {
			// 샷을 잘못 입력 시 재입력을 받습니다.
			shotInput = scanf('%d')
			if (coffeeMachine.addShot(shotInput)) break
		}

		log('농도를 몇단계 하시겠습니까? (총 0~3단계 까지)') // 농도 선택 후 추출
		while (true) {
			// 농도를 잘못 입력 시 재입력을 받습니다.
			densityInput = scanf('%d')
			if (coffeeMachine.selectDensity(densityInput)) break
		}

		await coffeeMachine.drop() //추출

		log('또 주문 하시겠습니까? (Y/N)') // 재시작: Y를 받으면 재시작 N이면 프로그램이 종료합니다.
		next = scanf('%s')
		next === 'Y' ? retry(coffeeMachine) : log('====See you Later movv...')
	}

	load()
}
